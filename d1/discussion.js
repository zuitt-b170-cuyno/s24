// $gt and $gte operator
// db.<collection-name>.find( { <field>: {$gt: <value>} } )
// db.<collection-name>.find( { <field>: {$gte: <value>} } )

db.users.find({ age: { $gt: 50 } })
db.users.find({ age: { $gte: 50 } })

// $lt and $lte operator
// db.<collection-name>.find( { <field>: {$lt: <value>} } )
// db.<collection-name>.find( { <field>: {$lte: <value>} } )

db.users.find({ age: { $lt: 50 } }).pretty()
db.users.find({ age: { $lte: 50 } }).pretty()

// $ne operator
// db.<collection-name>.find( { <field>: {$ne: <value>} } )

db.users.find({ age: { $ne: 50 } }).pretty()

// $in operator
db.users.find( { lastName: { $in: ["Hawking", "Doe"] } } ).pretty()

// $or operator
db.users.find( { $or: [{ firstName: "Neil" }, { age: "25" }] } ).pretty()

// $and operator
db.users.find( { $and: [{ age: { $ne: 82 } }, { age: { $ne: 76 } }] } ).pretty()

db.collection.insertOne(
    {
        name: "Philippines",
        capital: "Manila",
        continent: "Asia",
        language: ["Filipino", "English"],
        population: 112161766
    }
)

// Field projection
// Retrieving documents are common operations
// By default, MongoDB queries return the whole document as a response

/* 
    When dealing with complex data structure, there might be instances when fields
    are not useful for the query we are trying to accomplish
*/

// db.<collection-name>.find({ criteria }, { field: [0-1] })
// In { field: 1 }, 1 means the field is included in the query. Except for the _id, the rest are excluded
db.users.find(
    { firstName: "Jane" },
    {
        firstName: 1,
        lastName: 1,
    }
).pretty()

// In { field: 0 }, 0 means the field is explicitly excluded in the query.
db.users.find(
    { firstName: "Jane" },
    {
        contact: 0,
        department: 0,
        _id: 0
    }
).pretty()

// Suppressing the field
// _id is always included in the query. The following explicitly exludes the _id
db.users.find(
    { firstName: "Jane" },
    {
        firstName: 1, 
        lastName: 1,
        contact: 1,
        _id: 0
    }
)

// Embedded Approach
const movie = {
    id: 12345,
    title: "Titanic",
    genre: "Romance",
    reviews: [
        {
            name: "Johnny",
            rating: "5",
            comments: "Lorem Ipsum"
        },
        {
            name: "Jam",
            rating: "5",
            comments: "Lorem Ipsum"
        }
    ]
}

// Referencing Approach
const movies = {
    id: 12345,
    title: "Titanic",
    genre: "Romance",
    reviews: [
        {
            review_id: 1
        },
        {
            review_id: 2
        }
    ]
}

const reviews = [
    {
        id: 1, 
        name: "Johnny",
        rating: "5",
        comments: "Lorem Ipsum"
    },
    {
        id: 2, 
        name: "Jam",
        rating: "5",
        comments: "Lorem Ipsum"
    },
]

const course = {

}

// $regex operator
// Allows us to find documents that match the specific string pattern using regular expression
// Syntax: db.<collection-name>.find({ <field>: { $regex: <pattern>, $options: <option> } })

// Case sensitive query
db.users.find( { firstName: { $regex: 'N' } } )

// Case insensitive query
db.users.find( { firstName: { $regex: 'j', $options: '$i' } } ).pretty()